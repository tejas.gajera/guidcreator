﻿using System;
using System.IO;

namespace GuidCreator
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            const int numberOfGuids = 1000;
            using (var file = new StreamWriter(@"Guids.txt"))
            {
                for (var i = 0; i < numberOfGuids; i++)
                    file.WriteLine(Guid.NewGuid());

                file.Close();
            }

            Console.WriteLine($"Successfully generated {numberOfGuids} unique guids!");
            Console.ReadKey();
        }
    }
}
